package net.vanheesch.kismetclient.model;

import java.util.Date;

public class Probe {

    private String mac;
    private Date captureTime;
    private int signalStrength;

    public Probe(String mac, Date captureTime, int signalStrength) {
        this.mac = mac;
        this.captureTime = captureTime;
        this.signalStrength = signalStrength;
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    public Date getCaptureTime() {
        return captureTime;
    }

    public void setCaptureTime(Date captureTime) {
        this.captureTime = captureTime;
    }

    public int getSignalStrength() {
        return signalStrength;
    }

    public void setSignalStrength(int signalStrength) {
        this.signalStrength = signalStrength;
    }

}
