package net.vanheesch.kismetclient.model;

public class Device {

    private String mac = null;
    private String name = null;
    private String type = null;

    public Device(String mac) {
        this.setMac(mac);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

}
