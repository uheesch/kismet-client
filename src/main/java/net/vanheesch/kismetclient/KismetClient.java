package net.vanheesch.kismetclient;

import net.vanheesch.kismetclient.db.MySQLDAO;
import net.vanheesch.kismetclient.model.Device;
import net.vanheesch.kismetclient.model.Probe;

import java.io.*;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Date;
import java.util.HashMap;

public class KismetClient {

    private Inet4Address kismetServerAddress;
    private int port;
    private Socket clientSocket;
    private BufferedReader socketReader;
    private BufferedWriter socketWriter;
    private Date currentTime;

    private HashMap<String, String> vendorMap = new HashMap<>();

    private MySQLDAO mySQLDAO;

    public KismetClient(Inet4Address kismetServerAddress, int port) {
        this.kismetServerAddress = kismetServerAddress;
        this.port = port;
        mySQLDAO = new MySQLDAO();
        connect();
        scanWifiClients();
    }

    public static void main(String[] args) {
        String kismetServerIP;
        if (args.length == 0 || args[0] == null) {
            kismetServerIP = "127.0.0.1";
        } else {
            kismetServerIP = args[0];
        }
        try {
            System.out.println("Starting");
            new KismetClient(
                    (Inet4Address) InetAddress.getByName(kismetServerIP),
                    2501);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

    }

    private void connect() {
        try {
            clientSocket = new Socket(this.kismetServerAddress, this.port);
            socketWriter = new BufferedWriter(new OutputStreamWriter(
                    clientSocket.getOutputStream()));
            socketReader = new BufferedReader(new InputStreamReader(
                    clientSocket.getInputStream()));
        } catch (IOException e) {
            e.printStackTrace();
            if (clientSocket != null) {
                try {
                    clientSocket.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        }
    }

    private void scanWifiClients() {
        try {
            // socketWriter.write("!1 REMOVE time\r\n");
            socketWriter.write("!1 ENABLE CLIENT mac,signal_dbm\r\n");
            socketWriter.flush();
            String line;
            while ((line = socketReader.readLine()) != null) {

                if (line.startsWith("*TIME")) {
                    this.currentTime = parseDate(line);
                } else if (line.startsWith("*CLIENT")) {
                    parseClient(line);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private Date parseDate(String line) {
        return new Date(Long.parseLong(line.split(":")[1].trim()) * 1000);
    }

    private void parseClient(String line) {
        String[] tokens = line.split(" ");
        String clientMAC = tokens[1].trim();
        int signalDbm = Integer.parseInt(tokens[2].trim());

        Device device = new Device(clientMAC);
        if (!mySQLDAO.deviceExists(device)) {
            String vendorMacPart = clientMAC.substring(0, 8);
            String vendor = vendorMap.get(vendorMacPart);
            if (vendor != null) {
                device.setType(vendor);
            } else {
                vendor = MacVendorService.lookupVendor(clientMAC);
                vendorMap.put(vendorMacPart, vendor);
            }
            device.setType(vendor);
            mySQLDAO.addDevice(device);
        }

        if (currentTime != null) {
            Probe probe = new Probe(clientMAC, currentTime, signalDbm);
            mySQLDAO.addProbe(probe);
        }

    }

}
