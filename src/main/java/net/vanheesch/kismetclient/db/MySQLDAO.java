package net.vanheesch.kismetclient.db;

import net.vanheesch.kismetclient.model.Device;
import net.vanheesch.kismetclient.model.Probe;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class MySQLDAO {

    private static final int CONNECTION_TEST_TIMEOUT = 3;
    private static final String PASSWORD = "jhg76jhg**__1";
    private static final String USER = "wifi";
    private static final String CONNECTION_STRING = "jdbc:mysql://host/wifitracking";

    static {
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Connection connection;

    private void connect() {
        try {
            if (connection == null
                    || !connection.isValid(CONNECTION_TEST_TIMEOUT)) {
                connection = DriverManager.getConnection(CONNECTION_STRING
                        + "?user=" + USER + "&password=" + PASSWORD);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void addProbe(Probe probe) {
        Object[] queryParameters = new Object[3];
        queryParameters[0] = probe.getMac();
        queryParameters[1] = probe.getCaptureTime();
        queryParameters[2] = probe.getSignalStrength();
        executeUpdate(
                "INSERT IGNORE INTO probe (mac,capture_time,signal_strength) VALUES (?,?,?)",
                queryParameters);
    }

    public boolean addDevice(Device device) {
        if (!deviceExists(device)) {
            Object[] queryParameters = new Object[2];
            queryParameters[0] = device.getMac();
            queryParameters[1] = device.getType();
            executeUpdate("INSERT INTO device (mac,type) VALUES (?,?)",
                    queryParameters);
            return true;
        }
        return false;
    }

    private void executeUpdate(String parameterizedSQLQuery,
                               Object... queryParameters) {
        PreparedStatement prepStatement = null;
        connect();
        try {
            prepStatement = connection.prepareStatement(parameterizedSQLQuery);
            for (int i = 1; i <= queryParameters.length; i++) {
                prepStatement.setObject(i, queryParameters[i - 1]);
            }
            prepStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (prepStatement != null) {
                    prepStatement.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public boolean deviceExists(Device device) {
        PreparedStatement prepStatement = null;
        connect();
        try {
            prepStatement = connection
                    .prepareStatement("SELECT * FROM device WHERE mac=?");
            prepStatement.setString(1, device.getMac());
            return prepStatement.executeQuery().next();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (prepStatement != null) {
                    prepStatement.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

}
